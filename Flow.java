package com.transvoyant.carrier.parsehub.stream.event.flow;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Flow<T> {
    private List<Consumer<T>> handlers = new ArrayList<>();

    public void put(T record) {
        handlers.forEach(handler ->
                handler.accept(record));
    }

    public void to(Flow<T> targetFlow) {
        handlers.add(targetFlow::put);
    }

    public Flow<T> copyTo(Flow<T> targetFlow) {
        Flow<T> newFLow = new Flow<>();
        handlers.add(record -> {
            newFLow.put(record);
            targetFlow.put(record);
        });
        return newFLow;
    }

    public <R> Flow<R> map(Function<T, R> mapper) {
        Flow<R> newFLow = new Flow<>();
        handlers.add((T record) ->
                newFLow.put(mapper.apply(record)));
        return newFLow;
    }

    public <R> Flow<R> flatMap(Function<T, Optional<R>> mapper) {
        Flow<R> newFLow = new Flow<>();
        handlers.add((T record) ->
                mapper.apply(record).ifPresent(newFLow::put));
        return newFLow;
    }

    public Flow<T> process(Consumer<T> consumer) {
        return map(record -> {
            consumer.accept(record);
            return record;
        });
    }

    public void ifToElseTo(Predicate<T> predicate1, Flow<T> ifFlow, Flow<T> elseFlow) {
        handlers.add(record -> {
            if (predicate1.test(record)) {
                ifFlow.put(record);
            } else {
                elseFlow.put(record);
            }
        });
    }

    public Flow<T> elseTo(Predicate<T> predicate, Flow<T> elseFlow) {
        Flow<T> newFLow = new Flow<>();
        ifToElseTo(predicate, newFLow, elseFlow);
        return newFLow;
    }
}
