package com.transvoyant.carrier.parsehub.stream.event.flow;

import java.time.Duration;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.SECONDS;

public class DelayedFlow<T> extends Flow<T> {
    private final ScheduledExecutorService executorService;
    private final Duration defaultDelay;

    public DelayedFlow(ScheduledExecutorService executorService, Duration defaultDelay) {
        super();
        this.defaultDelay = defaultDelay;
        this.executorService = executorService;
    }

    @Override
    public void put(T record) {
        executorService.schedule(
                () -> super.put(record),
                defaultDelay.getSeconds(), SECONDS);
    }
}
